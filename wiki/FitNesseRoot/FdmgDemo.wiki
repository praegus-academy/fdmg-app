---
Suite
---
!1 FDMG Demo Suite
!*> FitNesse Setup
!define TEST_SYSTEM {slim}
!define ALL_UNCLE_SUITE_SETUPS {true}
!path fixtures
!path fixtures/*.jar
*!

!contents -R2 -g -p -f -h