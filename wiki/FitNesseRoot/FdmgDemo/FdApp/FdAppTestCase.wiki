---
Test
---
!*> geheim
|script           |string fixture |
|$SECRET_password=|value of|s3cr3t|
*!

!|script                       |android test                                                                           |
|accepteer cookiewall                                                                                                  |
|open eerste item van categorie|Economie                                                                               |
|ensure                        |is visible      |Log in en lees:                                                       |
|click                         |Inloggen                                                                               |
|enter                         |test@test.net   |as                            |E-mail                                 |
|enter                         |$SECRET_password|as                            |Wachtwoord                             |
|click                         |Inloggen                                                                               |
|check                         |value of        |id=nl.fdmg.fd:id/error_message|Uw email en/of wachtwoord is niet juist|